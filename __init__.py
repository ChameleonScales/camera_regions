# ##### BEGIN GPL LICENSE BLOCK #####
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License version 3 as published by
#	the Free Software Foundation.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
	"name": "Camera Regions",
	"author": "Caetano Veyssières (ChameleonScales)",
	"version": (1, 2, 3),
	"blender": (4, 0, 2),
	"location": "Properties Area > Camera Data > Regions",
	"description": "Adds and stores camera regions based on render regions",
	"doc_url": "https://gitlab.com/ChameleonScales/camera-cropper",
	"tracker_url":"https://gitlab.com/ChameleonScales/camera-cropper/issues",
	"warning": "",
	"category": "Camera"}


import bpy
import os
from bpy.props import *
from bpy.types import Panel, AddonPreferences
from math import trunc

def prefs():
	return bpy.context.preferences.addons[__name__].preferences

def store_region_in_cam(self,context,name,Xm,XM,Ym,YM):
	# Adds or replaces custom properties in active camera's data:
	Sc = bpy.context.scene
	CamD = bpy.context.object.data

	new_region = CamD.regions.add()
	new_region.name = name
	new_region.sensor = CamD.sensor_width
	new_region.renderX = Sc.render.resolution_x
	new_region.renderY = Sc.render.resolution_y
	new_region.shiftX = CamD.shift_x
	new_region.shiftY = CamD.shift_y
	new_region.orthoScale = CamD.ortho_scale
	new_region.lam = CamD.latitude_min
	new_region.laM = CamD.latitude_max
	new_region.lom = CamD.longitude_min
	new_region.loM = CamD.longitude_max
	new_region.regXm = Xm
	new_region.regXM = XM
	new_region.regYm = Ym
	new_region.regYM = YM

def ShowMessageBox(title = "Message Box", icon = 'INFO', lines=""):
    myLines=lines
    def draw(self, context):
        for n in myLines:
            self.layout.label(text=n)
    bpy.context.window_manager.popup_menu(draw, title = title, icon = icon)

# 'Add Region' operator:
class OBJECT_OT_add_region(bpy.types.Operator):
	bl_idname = "regions.add_region"
	bl_label = "Add Region"
	bl_description = "Adds a camera region based on the current render region"
	bl_options = {'REGISTER', 'UNDO'}

	region_name : bpy.props.StringProperty(name="Region Name")

	def execute(self, context):
		user_input = self.region_name
		CamD = bpy.context.object.data
		if CamD.type == 'PANO' and CamD.panorama_type != 'EQUIRECTANGULAR':
			myLines=("Panoramic cameras other than",
					"Equirectangular are not supported.",
					"Operation cancelled.")
			ShowMessageBox(title="Sorry!", icon='ERROR', lines=myLines)

			self.report({'INFO'}, "Operation canceled : unsupported camera type")
		else:
			# add Original region if it doesn't already exist:
			if 'Original' not in bpy.context.object.data.regions.keys() :
				store_region_in_cam(self,context,'Original',0,1,0,1)
			regionName = user_input

			Sc = bpy.context.scene
			X = Sc.render.resolution_x
			Y = Sc.render.resolution_y
			Xm = Sc.render.border_min_x
			XM = Sc.render.border_max_x
			Ym = Sc.render.border_min_y
			YM = Sc.render.border_max_y

			# Snap border values to the pixel grid
			Xm = trunc(Xm*X)/X
			XM = trunc(XM*X)/X
			Ym = trunc(Ym*Y)/Y
			YM = trunc(YM*Y)/Y

			#------------------------------------------------------------------------
			#					UN-CROPPED SCENE CREATION PART:
			#------------------------------------------------------------------------
			# Disable Region Render:
			Sc.render.use_border=False

			NewSc = Sc.copy()
			NewSc.use_fake_user = True
			# Empty the copied scene:
			for col in NewSc.collection.children:
				NewSc.collection.children.unlink(col)
			for obj in NewSc.collection.objects:
				NewSc.collection.objects.unlink(obj)

			NewSc.name = f'{Sc.name} {CamD.name} {regionName}'
			NewSc.use_nodes = True
			NewSc.node_tree.nodes["Render Layers"].scene = bpy.data.scenes[Sc.name]
			Nodes = NewSc.node_tree.nodes

			# Container nodegroup:
			containerGrp = Nodes.new('CompositorNodeGroup')
			containerGrp.node_tree = bpy.data.node_groups.new(regionName + ' Position','CompositorNodeTree')
			containerGrpTree = containerGrp.node_tree
			containerGrpTreeIn = containerGrpTree.nodes.new('NodeGroupInput')
			containerGrpTree.interface.new_socket('Image', in_out='OUTPUT', socket_type='NodeSocketColor')
			containerGrpTreeOut = containerGrpTree.nodes.new('NodeGroupOutput')
			containerGrpTree.interface.new_socket('Rendered region', in_out='INPUT', socket_type='NodeSocketColor')

			rep = containerGrpTree.nodes.new('CompositorNodeGroup')
			comp = Nodes['Composite']
			RL = Nodes['Render Layers']
			# path to the blend file containing template Reposition nodegroup:
			templatePath = os.path.dirname(os.path.abspath(__file__))+"/reposition.blend"
			# append nodegroup to scene (False means it appends instead of linking):
			with bpy.data.libraries.load(templatePath, link=False) as (data_from, data_to):
				data_to.node_groups = ["Reposition"]
			# assign it to the previously added nogegroup:
			rep.node_tree = bpy.data.node_groups['Reposition']
			# Assign values to the Reposition nodegroup:
			rep.inputs['Canvas X'].default_value = X
			rep.inputs['region X min'].default_value = Xm
			rep.inputs['region X Max'].default_value = XM
			rep.inputs['Canvas Y'].default_value = Y
			rep.inputs['region Y min'].default_value = Ym
			rep.inputs['region Y Max'].default_value = YM

			# Rearrange and connect nodes:
			# Socket currently connected to the Compositor output:
			out2comp = comp.inputs['Image'].links[0].from_socket

			containerGrp.location[0] = comp.location[0]
			containerGrp.location[1] = comp.location[1]
			containerGrpTreeIn.location[0] = -230
			containerGrpTreeIn.location[1] = -50
			containerGrpTreeOut.location[0] = 300
			rep.width = 200
			comp.location[0] += 230
			NewSc.node_tree.links.new(out2comp, containerGrp.inputs["Rendered region"])
			containerGrpTree.links.new(containerGrpTreeIn.outputs['Rendered region'], rep.inputs['Rendered region'])
			containerGrpTree.links.new(rep.outputs['Image'], containerGrpTreeOut.inputs['Image'])
			NewSc.node_tree.links.new(containerGrp.outputs['Image'], comp.inputs['Image'])
			NewSc.node_tree.links.new(rep.outputs["Image"], comp.inputs["Image"])
			containerGrp.hide = True
			RL.mute = True

			#------------------------------------------------------------------------
			#						CAMERA CROPPING PART:
			#------------------------------------------------------------------------

			# Region Ranges:
			XR = XM-Xm
			YR = YM-Ym

			# Crop Perspective camera:
			if CamD.type == 'PERSP':
				sW = CamD.sensor_width
				# Sensor size change (using sensor size instead of focal length preserves depth of field):
				if (X>Y):
					if ((X*XR)>((Y*YR))):
						CamD.sensor_width = sW / (1/XR)
					else:
						CamD.sensor_width = sW / (X/(Y * YR))
				else:
					if ((X*XR)>((Y*YR))):
						CamD.sensor_width = sW / (Y/(X * XR))
					else :
						CamD.sensor_width = sW / (1/YR)
				# Resolution change:
				Sc.render.resolution_x = round(X * XR)
				Sc.render.resolution_y = round(Y * YR)
				# Shift X:
				# if camera has landscape ratio
				if ((X * XR)>(Y * YR)):
					CamD.shift_x = (0.5 * (XM + Xm - 1)) / XR
				# else : camera has portrait ratio
				else :
					CamD.shift_x = (0.5 * (X * XR) * (XM + Xm - 1))/((Y * YR) * XR)
				# Shift Y
				if ((X * XR)>(Y * YR)):
					CamD.shift_y = (0.5 * (Y * YR) * (YM + Ym - 1))/((X * XR) * YR)
				else :
					CamD.shift_y = (0.5 * (YM + Ym - 1)) / YR

			# Crop Orhtographic camera:
			elif CamD.type == 'ORTHO':
				Os = CamD.ortho_scale
				# Orthographic scale change:
				if (X>Y):
					if ((X*XR)>((Y*YR))):
						CamD.ortho_scale = Os * XR
					else:
						CamD.ortho_scale = Os / (X/(Y*YR))
				else:
					if ((X*XR)>((Y*YR))):
						CamD.ortho_scale = Os / (Y/(X*XR))
					else :
						CamD.ortho_scale = Os * YR
				# Resolution change:
				Sc.render.resolution_x = round(X * XR)
				Sc.render.resolution_y = round(Y * YR)
				# Shift X:
				# if camera has landscape ratio
				if ((X * XR)>(Y * YR)):
					CamD.shift_x = (0.5 * (XM + Xm - 1)) / XR
				# else : camera has portrait ratio
				else :
					CamD.shift_x = (0.5 * (X * XR) * (XM + Xm - 1))/((Y * YR) * XR)
				# Shift Y:
				if ((X * XR)>(Y * YR)):
					CamD.shift_y = (0.5 * (Y * YR) * (YM + Ym - 1))/((X * XR) * YR)
				else :
					CamD.shift_y = (0.5 * (YM + Ym - 1)) / YR

			# Crop Equirectangular camera:
			elif CamD.panorama_type == 'EQUIRECTANGULAR':
				lam = CamD.latitude_min
				laM = CamD.latitude_max
				lom = CamD.longitude_min
				loM = CamD.longitude_max
				# latitude & longitude ranges:
				laR = laM - lam
				loR = loM - lom
				# Resolution change:
				Sc.render.resolution_x = round(X * XR)
				Sc.render.resolution_y = round(Y * YR)
				# Longitude change:
				CamD.longitude_min = lom+Xm*(loR)
				CamD.longitude_max = loM-(1-XM)*(loR)
				# Latitude change
				CamD.latitude_min = lam+Ym*(laR)
				CamD.latitude_max = laM-(1-YM)*(laR)

			store_region_in_cam(self,context,regionName,Xm,XM,Ym,YM)
			if prefs().create_camera:
				CamD2 = bpy.context.object.data.copy()
				CamObj1 = bpy.context.object
				CamObj2 = CamObj1.copy()
				CamObj2.data = CamD2
				Cam2Name = regionName
				if prefs().include_dimensions:
					Cam2Name = f"{Cam2Name}_{str(Sc.render.resolution_x)}x{str(Sc.render.resolution_y)}"
				if prefs().include_orig_cam_name:
					Cam2Name = f"{CamObj1.name}_{Cam2Name}"
				CamObj2.name = Cam2Name
				CamD2.name = Cam2Name
				bpy.context.collection.objects.link(CamObj2)
				bpy.context.object.data.active_region=0
				bpy.ops.regions.jump_to()
			# Info report
			self.report({'INFO'}, "Added Region + Scene")

		return {'FINISHED'}

	def invoke(self, context, event):
		wm = context.window_manager
		CR = context.scene.CR_props
		if context.active_object:
			# Determine region name based on already existing:
			n = 1
			names = bpy.context.object.data.regions.keys()
			prefix = CR.region_prefix
			if not names :
				self.region_name = prefix + ' 1'
			else :
				for name in names :
					while f'{prefix} {str(n)}' in name :
						n += 1
					self.region_name = f'{prefix} {str(n)}'

		return wm.invoke_props_dialog(self)

	def draw(self, context):
		row = self.layout
		row.row()
		row.prop(self, "region_name")
		row.row()


class CAMERA_OT_jump_to_region(bpy.types.Operator):
	bl_idname = "regions.jump_to"
	bl_label = "Jump To Region"
	bl_description = "Jump to the selected region in the list"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		Sc=bpy.context.scene
		CamD = bpy.context.object.data
		ID = CamD.active_region
		active_region = CamD.regions[ID]

		CamD.sensor_width = active_region.sensor
		Sc.render.resolution_x = active_region.renderX
		Sc.render.resolution_y = active_region.renderY
		CamD.shift_x = active_region.shiftX
		CamD.shift_y = active_region.shiftY
		CamD.ortho_scale = active_region.orthoScale
		CamD.latitude_min = active_region.lam
		CamD.latitude_max = active_region.laM
		CamD.longitude_min = active_region.lom
		CamD.longitude_max = active_region.loM
		# Info report
		self.report({'INFO'}, "Jumped to region " + active_region.name)

		return {'FINISHED'}


class CAMERA_OT_delete_region(bpy.types.Operator):
	bl_idname = "regions.delete_region"
	bl_label = "Delete Region"
	bl_description = "Delete the selected region in the list"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		CamD = bpy.context.object.data
		ID = CamD.active_region
		active_region_name = CamD.regions[ID].name

		bpy.context.object.data.regions.remove(ID)

		# Info report
		self.report({'INFO'}, "deleted region " + active_region_name)

		return {'FINISHED'}


class CAMERA_OT_regions_to_text(bpy.types.Operator):
	bl_idname = "regions.to_text"
	bl_label = "Write data"
	bl_description = "Write Regions data in the text editor"
	bl_options = {'REGISTER', 'UNDO'}

	def execute(self, context):
		txt = bpy.data.texts.new('Camera Regions Data')
		canvX = bpy.context.object.data.regions[0].renderX
		canvY = bpy.context.object.data.regions[0].renderY
		CamD = bpy.context.object.data

		for r in context.camera.regions:
			offsetX = canvX * (((r.regXM-r.regXm)/2)+r.regXm-0.5)
			offsetY = canvY * (((r.regYM-r.regYm)/2)+r.regYm-0.5)
			posX = r.regXm * canvX
			posY = (1-r.regYM) * canvY
			txt.write(f"\n {r.name}:")
			if CamD.panorama_type == 'EQUIRECTANGULAR':
				txt.write("\n    Latitude min: " + str(r.lam))
				txt.write("\n    Latitude Max: " + str(r.laM))
				txt.write("\n    Longitude min: " + str(r.lom))
				txt.write("\n    Longitude Max: " + str(r.loM))
			if CamD.type == 'ORTHO':
				txt.write("\n    Orthographic Scale: " + str(r.orthoScale))
			if CamD.type == 'PERSP':
				txt.write("\n    Sensor size: " + str(r.sensor))
			if CamD.type == 'PERSP' or CamD.type == 'ORTHO':
				txt.write("\n    Shift X: " + str(r.shiftX))
				txt.write("\n    Shift Y: " + str(r.shiftY))
			txt.write("\n    Image Width: " + str(r.renderX))
			txt.write("\n    Image Height: " + str(r.renderY))
			txt.write("\n    Region X min: " + str(r.regXm))
			txt.write("\n    Region X Max: " + str(r.regXM))
			txt.write("\n    Region Y min: " + str(r.regYm))
			txt.write("\n    Region Y Max: " + str(r.regYM))
			txt.write("\n    Pos. from center X: " + str(round(offsetX,1)))
			txt.write("\n    Pos. from center Y: " + str(round(offsetY,1)))
			txt.write("\n    Pos. from top-left X: " + str(round(posX)))
			txt.write("\n    Pos. from top-left Y: " + str(round(posY)))
		txt.current_line_index = 0
		txt.current_character = 0
		txt.select_end_line_index = 0
		txt.select_end_character = 0
		# Info report
		self.report({'INFO'}, "Wrote Regions data to text ")

		return {'FINISHED'}


class CAMERA_UL_region_slots(bpy.types.UIList):
	# The draw_item function is called for each item of the collection that is visible in the list.
	#   data is the RNA object containing the collection,
	#   item is the current drawn item of the collection,
	#   icon is the "computed" icon for the item (as an integer, because some objects like materials or textures
	#   have custom icons ID, which are not available as enum items).
	#   active_data is the RNA object containing the active property for the collection (i.e. integer pointing to the
	#   active item of the collection).
	#   active_propname is the name of the active property (use 'getattr(active_data, active_propname)').
	#   index is index of the current item in the collection.
	#   flt_flag is the result of the filtering process for this item.
	#   Note: as index and flt_flag are optional arguments, you do not have to use/declare them here if you don't
	#		 need them.
	def draw_item(self, context, layout, data, item, icon, active_data, active_propname):
		ob = data
		reg = item
		# draw_item must handle the three layout types... Usually 'DEFAULT' and 'COMPACT' can share the same code.
		if self.layout_type in {'DEFAULT', 'COMPACT'}:
			# You should always start your row layout by a label (icon + text), or a non-embossed text field,
			# this will also make the row easily selectable in the list! The later also enables ctrl-click rename.
			# We use icon_value of label, as our given icon is an integer value, not an enum ID.
			# Note "data" names should never be translated!
			if reg:
				layout.prop(reg, "name", text="", emboss=False)
			else:
				layout.label(text="whatsup", translate=False)
		# 'GRID' layout type should be as compact as possible (typically a single icon!).
		elif self.layout_type in {'GRID'}:
			layout.alignment = 'CENTER'
			layout.label(text="", icon_value=icon)


CameraRegionsProperties = type(
	"CameraRegionsProperties",
	(bpy.types.PropertyGroup,), {
		"__annotations__": {

			"region_prefix" : StringProperty(
				name = 'Region name prefix',
				default = "Region"
				),
		},
	},
)

# And now we can use this list everywhere in Blender. Here is a small example panel.
class CAMERA_PROPERTIES_UI_Panel(bpy.types.Panel):
	"""Creates a Panel in the Camera Data tab of the properties window"""
	bl_label = "Regions"
	bl_idname = "DATA_PT_ui_list_regions"
	bl_space_type = 'PROPERTIES'
	bl_region_type = 'WINDOW'
	bl_context = "data"

	def draw(self, context):
		CR = context.scene.CR_props
		if bpy.context.object.type == 'CAMERA':
			layout = self.layout
			layout.use_property_split = True
			CamD = context.object.data
			col = layout.column()
			col.prop(CR, 'region_prefix')
			col.separator(factor=2.0)
			split = col.split(factor = 0.58, align = True)
			split.operator('regions.add_region', icon='PLUS')
			col = layout.column()
			split = col.split(factor = 0.6, align = True)
			split.template_list(
								listtype_name = "CAMERA_UL_region_slots",
								list_id = "",
								dataptr = CamD,
								propname = "regions",
								active_dataptr = CamD,
								active_propname = "active_region",
								)
			col = split.column()
			col.enabled = len(context.object.data.regions) > 0
			col.operator('regions.jump_to', icon='BORDERMOVE')
			col.separator(factor=2.0)
			row = col.row()
			row.enabled = context.object.data.active_region != 0
			row.operator('regions.delete_region', icon='TRASH')
			col.separator(factor=2.0)
			col.operator('regions.to_text', icon='TEXT')
	@classmethod
	def poll(cls, context):
		return context.object.type == 'CAMERA'


class OUTPUT_PROPERTIES_UI_Panel(bpy.types.Panel):
	"""Creates a Panel in the Camera Data tab of the properties window"""
	bl_label = "Render Region"
	bl_idname = "DATA_PT_ui_numeric_region"
	bl_space_type = 'PROPERTIES'
	bl_region_type = 'WINDOW'
	bl_context = "output"

	def draw(self, context):
		Sc = context.scene
		layout = self.layout
		layout.use_property_split = True

		col = layout.column()
		split = col.split(factor=0.2, align=True)
		col1 = split.row(align = True)
		col2 = split.row(align = True)
		split = col2.split(factor=0.7, align=True)
		split.prop(Sc.render, 'border_max_y', text='Ymax')
		col3 = split.row(align=True)

		col = layout.column()
		split = col.split(factor=0.5, align=True)
		split.prop(Sc.render, 'border_min_x', text='Xmin')
		split.prop(Sc.render, 'border_max_x', text='Xmax')

		col = layout.column()
		split = col.split(factor=0.2, align=True)
		col1 = split.row(align = True)
		col2 = split.row(align = True)
		split = col2.split(factor=0.7, align=True)
		split.prop(Sc.render, 'border_min_y', text='Ymin')
		col3 = split.row(align=True)


class Camera_Regions(bpy.types.PropertyGroup):
	name: bpy.props.StringProperty(name='the name', default='Region')
	sensor: bpy.props.FloatProperty(name='sensor size')
	renderX: bpy.props.IntProperty(name='Render Width')
	renderY: bpy.props.IntProperty(name='Render Height')
	shiftX: bpy.props.FloatProperty(name='Shift X')
	shiftY: bpy.props.FloatProperty(name='Shift Y')
	orthoScale: bpy.props.FloatProperty(name='Ortho Scale')
	lam: bpy.props.FloatProperty(name='Latitude min')
	laM: bpy.props.FloatProperty(name='Latitude Max')
	lom: bpy.props.FloatProperty(name='Longitude min')
	loM: bpy.props.FloatProperty(name='Longitude Max')
	regXm: bpy.props.FloatProperty(name='Region X min')
	regXM: bpy.props.FloatProperty(name='Region X Max')
	regYm: bpy.props.FloatProperty(name='Region Y min')
	regYM: bpy.props.FloatProperty(name='Region Y Max')


class CameraRegionsPrefs(bpy.types.AddonPreferences):
	bl_idname = __name__

	create_camera : BoolProperty(
		name="Create camera",
		description='if unchecked, will keep a single camera and jump to the region',
		default = False,
		)
	include_dimensions : BoolProperty(
		name="Include region dimensions",
		description="Includes the region's pixel dimensions in the generated camera's name",
		default = False,
		)
	include_orig_cam_name : BoolProperty(
		name="Include original camera name",
		description="Includes the original camera's name in the generated camera's name",
		default = False,
		)

	def draw(self, context):
		layout = self.layout
		#layout.use_property_split = True
		row = layout.row(align=True)
		row.label(text="Create a camera for each region and keep the current view")
		row.prop(self, 'create_camera', text="")

		row = layout.row(align=True)
		row.enabled = self.create_camera == True
		row.label(text="Include region dimensions in the generated camera's name")
		row.prop(self, 'include_dimensions', text="")

		row = layout.row(align=True)
		row.enabled = self.create_camera == True
		row.label(text="Include the original camera's name in the generated one")
		row.prop(self, 'include_orig_cam_name', text="")



# ------------------------------------------------------
# Registration
# ------------------------------------------------------
classes = (
	OBJECT_OT_add_region,
	CAMERA_OT_jump_to_region,
	CAMERA_OT_delete_region,
	CAMERA_OT_regions_to_text,
	CAMERA_UL_region_slots,
	CAMERA_PROPERTIES_UI_Panel,
	OUTPUT_PROPERTIES_UI_Panel,
	Camera_Regions,
	CameraRegionsProperties,
	CameraRegionsPrefs,
)

def register():
	from bpy.utils import register_class
	for cls in classes:
		register_class(cls)
	bpy.types.Scene.CR_props = PointerProperty(type=CameraRegionsProperties)
	bpy.types.Camera.regions = bpy.props.CollectionProperty(
															type=Camera_Regions,
															name="Camera regions",
															description="the camera regions",
															)
	bpy.types.Camera.active_region = bpy.props.IntProperty(name='active region ID')


def unregister():
	del bpy.types.Scene.CR_props
	from bpy.utils import unregister_class
	for cls in reversed(classes):
		unregister_class(cls)


if __name__ == "__main__":
	register()
