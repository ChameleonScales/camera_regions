# Camera Regions

![logo](https://gitlab.com/ChameleonScales/camera_regions/-/raw/master/logo.png)

### This Blender add-on lets you:
- Convert the <kbd>Ctrl</kbd>+<kbd>B</kbd> render region to the camera's frame itself (a "camera region")
- Store and access as many of these regions as you want in a camera's properties
- Export region data to text
- Place a region render back into the original canvas


## Why though?

At least 2 reasons:
- address the issue that most (if not all) network rendering tools and renderfarms can't use region render. With this they can.
- batch render multiple regions by combining this add-on with this other one https://github.com/luckykadam/render-strip

# How to use it:


Full video tutorial (8 min) (slightly older version): https://odysee.com/@ChameleonScales:c/blender-camera-regions-tutorial

Simplified:

1. select your region (Ctrl+B)
3. Go in your camera properties > Regions panel
4. Click on Add region
5. Render or send your file to a renderfarm

Once the region is rendered, you can place it back in the original canvas:

6. Jump to the scene that has the name of your region
7. Render
8. Go in the compositor and open an Image Viewer area with the render result
9. Either unmute the Render layers node or add an image node with the image you got back from your renderfarm

and Voilà!

⚠ There is currently a hard limit on Camera Shift X and Y (range goes from -10 to 10). If a region requires values beyond that, they will be positionned incorrectly. Follow the devtalk discussion here: https://devtalk.blender.org/t/camera-shift-hard-limit-causes-problem-with-my-add-on/25338

ℹ This add-on is distribued under the GPL version 3 license.

## TODO:
- Display regions data in the panel
- overlay the regions and their data in the 3D view, with customization options
- enable editing and replacing of regions
- if possible, automate scene renaming and deletion when region is renamed or deleted
